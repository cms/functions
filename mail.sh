# Useful functions for command line email

mailbody() {
    BODY=${BODY:-1}
    cat << EOF

MIME-Version: 1.0
Content-Type: multipart/mixed;

--body
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: quoted-printable
$BODY
--body
EOF
}
mailfile() {
    FILE_ATTACHMENT=$@
    B=$RANDOM
    cat << EOF
    
--bd$B
Content-type:Application/Octet-Stream;name=${FILE_ATTACHMENT};type=Binary
Content-transfer-encoding: base64
Content-disposition:attachment;name=${FILE_ATTACHMENT};filename=${FILE_ATTACHMENT}

 
EOF
    uuencode -mr ${FILE_ATTACHMENT} ${FILE_ATTACHMENT##*/}
    echo "--bd$B"
}
